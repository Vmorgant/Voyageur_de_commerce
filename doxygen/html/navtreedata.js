var NAVTREE =
[
  [ "Voyageur de commerce", "index.html", [
    [ "Fichiers", null, [
      [ "Liste des fichiers", "files.html", "files" ],
      [ "Variables globale", "globals.html", [
        [ "Tout", "globals.html", null ],
        [ "Fonctions", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"files.html"
];

var SYNCONMSG = 'cliquez pour désactiver la synchronisation du panel';
var SYNCOFFMSG = 'cliquez pour activer la synchronisation du panel';