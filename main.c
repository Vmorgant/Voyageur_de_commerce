
/**
*\file main.c
*\brief Ce programme permet a l'utilisateur de saisir une liste de distance entre differentes villes afin de choisir le trajet le plus efficace afin de toutes les visiter.
*\version 1.0
*\author Victor Morgant
*\date 11/11/2017
*/
#include <stdio.h>
#include <stdlib.h>

void Afficher_Tab(int *tab, int taille){
    /**
     * \fn void Afficher_Tab(int *tab, int taille)
     * \brief Fonction permettant d'afficher un tableau utilis�e dans les tests
     * \param *tab : le tableau � afficher, int taille : la taille de ce tableau
     */
    int i;
    for(i=0; i<taille; i++)
    {

        printf(" \n valeur %i = %i \n",i,*(tab+i));
    }

}
void Afficher_Mat(int **tab, int taille){
    /**
     * \fn void Afficher_Mat(int **tab, int taille)
     * \brief Fonction permettant d'afficher une matrice utilis�e dans les tests
     * \param **tab : la matrice � afficher, int taille : la taille de cette matrice
     */
    int i;
    for(i=0; i<taille; i++)
    {
        printf(" \n Ligne %i : \n",i);
        Afficher_Tab(tab[i],taille);
        printf("\n");

    }

}

int Mintab(int *tab, int taille){
    /**
     * \fn int Mintab(int *tab, int taille)
     * \brief Fonction permettant de trouver le minimum d'un tableau dynamique
     * \param int *tab : le tableau dont on cherche le minimum ,int taille :  la taille de ce tableau
     * \return le minimum du tableau
     */

    int i=0;
    int min;
    min=*tab;

    for (i=1; i<taille; i++)
    {

        if (  ( *(tab+i) !=0 )&& (*(tab+i) < min) )
            min=*(tab+i);
    }

    return min;

}

int RechercheVal( int *tab, int taille, int val){
    /**
     * \fn int RechercheVal( int *tab, int taille, int val)
     * \brief Fonction permettant de rechercher une valeur dans un tableau et d'obtenir sa position dans le tableau
     * \param int *tab : le tableau dont on cherche le minimum int taille: la taille de ce tableau, int val : la valeur � rechercher.
     * \return la position dans le tableau ou -1 si la valeur n'est pas trouv�e
     */

    int i=0;
    int res;
    while ( (i < taille) && *(tab+i) != val)
    {
        i++;
    }
    if (i==taille)
        res=-1; /*La valeur ne se trouve pas dans le tableau*/
    else
        res=i;
    return res;

}

void VilleSuivante(int villeDep, int nbVille, int nbVilleRest, int *parcours, int *distanceTot, int **distance){
    /**
     * \fn void VilleSuivante(int villeDep, int nbVille, int nbVilleRest, int *parcours, int *distanceTot, int **distance)
     * \brief Fonction permettant de trouver la ville la destinatiojn suivante du voyageur
     * \param int villeDep : la ville pr�c�dente, int nbVille : le nombre de ville total, int nbVilleRest : le nombre de ville restant � visiter, int *parcours: l'ordre des villes � visiter, int *distanceTot : la distance totale parcourue par le voyageur, int **distance : la matrice des diff�rentes distances entre les villes
     */

    int ttmp[nbVilleRest];
    int i=0,j;
    int tmp,disTmp,indTmp;

    for(j=0; j<=nbVille; j++)
    {
        tmp=RechercheVal(parcours-(nbVille-nbVilleRest),nbVille,j);/*On lit � chaque fois tout le tableau parcours*/
        if(tmp==-1) //La ville n'a pas �t� visit�e
        {
            ttmp[i]=distance[villeDep][j];
            i++;
        }

    }

    disTmp=Mintab(ttmp,nbVilleRest);
    *distanceTot=*distanceTot+disTmp;
    indTmp=RechercheVal(distance[villeDep],nbVille,disTmp);
    *parcours=indTmp;

}
int main (void){

    int villeDep,nbVille=0,distanceTot=0,i,j,k,tmp;
    int **distance=NULL;
    int * parcours=NULL;

    printf("\nCe programme permet a l'utilisateur de saisir une liste de distance entre differentes villes afin de choisir le trajet le plus efficace afin de toutes les visiter\n\n ");

    /*Allocation des tableau*/

    do
    {
        printf("\nMerci de saisir le nombre de ville\n");
        scanf("%d",&nbVille);
    }
    while(nbVille<1);


    parcours=(int*)malloc(sizeof(int)*nbVille);
    distance = (int **)malloc(nbVille * sizeof(int*));

    for(i = 0; i < nbVille; i++)
    {
        distance[i] = (int *)malloc(nbVille * sizeof(int));
    }

    /*Saisie des distances*/
    for(i=0; i<nbVille; i++)
    {
        for(j=0; j<nbVille; j++)
        {
            if(i==j)
                distance[i][j]=0;
            else if(j>i)
            {
                do
                {
                    printf("\nMerci de saisir la distance entre %i et %i\n",i+1,j+1);/*L'utilisateur commence � la ville n�1 et les indices du tableau � 0*/
                    scanf("%d",&tmp);
                    distance[i][j]=tmp;
                }
                while(tmp<1);

            }
            else
                distance[i][j]=distance[j][i];
        }
    }

    //Afficher_Mat(distance,nbVille);// /*Test d'affichage de la matrice*/


    /*Calcul du parcours*/
    do
    {
        printf("\nMerci de saisir la ville de depart entre 1 et %i\n",nbVille);
        scanf("%d",&villeDep);
    }
    while(villeDep<1 || villeDep>nbVille);

    parcours[0]=villeDep-1;

    for(k=1; k<nbVille; k++)
    {
        VilleSuivante(parcours[k-1],nbVille,nbVille-k,parcours+k,&distanceTot,distance);
    }

    /*Affichage du r�sultat*/
    printf("\nLe parcours optimal fait %i \n",distanceTot);
    printf("\nIl passe par les villes :");
    for(k=0; k<nbVille; k++)
    {
        printf(" %i ",parcours[k]+1);
    }
    /*Lib�ration de la m�moire allou�e*/
    free(parcours);
    for(i=0; i<nbVille; i++)
    {
        free(distance[i]);
    }
    free(distance);

    return 0;

}
